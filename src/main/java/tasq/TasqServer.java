package tasq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Tasq Server のエントリーポイント。
 *
 * @author ryotan
 * @since 1.0
 */
@SpringBootApplication
@ComponentScan
@EntityScan
@EnableJpaRepositories
public class TasqServer {

    public static void main(String... args) {
        SpringApplication.run(TasqServer.class, args);
    }
}
