package tasq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tasq.domain.entity.Milestone;
import tasq.domain.service.MilestoneService;
import tasq.domain.validation.group.Update;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("milestone")
public class MilestoneController {

    @Autowired
    MilestoneService service;

    @RequestMapping(value = "list", method = GET)
    public Iterable<Milestone> list() {
        return service.list();
    }

    @RequestMapping(method = POST)
    @ResponseStatus(CREATED)
    public Milestone save(@Validated @RequestBody Milestone milestone) {
        return service.save(milestone);
    }

    @RequestMapping(value = "{id}", method = PUT)
    @ResponseStatus(OK)
    public Milestone update(@PathVariable Long id, @Validated(Update.class) @RequestBody Milestone milestone) {
        return service.save(milestone);
    }

    @RequestMapping(value = "{id}", method = GET)
    public Milestone show(@PathVariable Long id) {
        return service.show(id);
    }
}
