package tasq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tasq.domain.entity.Task;
import tasq.domain.service.TaskService;
import tasq.domain.validation.group.Update;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * @author ryotan
 * @since 1.0
 */
@RestController
@RequestMapping("task")
public class TaskController {

    @Autowired
    TaskService service;

    @RequestMapping(value = "list", method = GET)
    public Iterable<Task> list() {
        return service.list();
    }

    @RequestMapping(method = POST)
    @ResponseStatus(CREATED)
    public Task save(@Validated @RequestBody Task task) {
        return service.save(task);
    }

    @RequestMapping(value = "{id}", method = PUT)
    @ResponseStatus(OK)
    public Task update(@PathVariable Long id, @Validated(Update.class) @RequestBody Task task) {
        return service.save(task);
    }

    @RequestMapping(value = "{id}", method = GET)
    public Task show(@PathVariable Long id) {
        return service.show(id);
    }
}
