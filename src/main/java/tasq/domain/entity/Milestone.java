package tasq.domain.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.Data;
import tasq.domain.validation.group.Update;

/**
 * マイルストーンを表すエンティティ。
 *
 * @author ryotan
 * @since 1.0
 */
@Entity
@Data
public class Milestone {

    @Id
    @GeneratedValue
    @NotNull(groups = Update.class)
    private long milestoneId;

    @NotNull
    private String milestoneName;

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date deadline;
}
