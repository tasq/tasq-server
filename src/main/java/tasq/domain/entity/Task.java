package tasq.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;
import tasq.domain.validation.group.Update;

/**
 * 個々のタスクを表すエンティティ。
 *
 * @author ryotan
 * @since 1.0
 */
@Entity
@Data
public class Task {

    @Id
    @GeneratedValue
    @NotNull(groups = Update.class)
    private Long taskId;

    @Column
    @NotNull
    private String title;

    @Column
    @NotNull
    private String description;
}
