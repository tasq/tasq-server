package tasq.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tasq.domain.entity.Milestone;

@Repository
public interface MilestoneRepository extends JpaRepository<Milestone, Long> {
}
