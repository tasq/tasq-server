package tasq.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tasq.domain.entity.Task;

/**
 * @author ryotan
 * @since 1.0
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

}
