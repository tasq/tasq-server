package tasq.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tasq.domain.entity.Milestone;
import tasq.domain.repository.MilestoneRepository;

@Service
@Transactional
public class MilestoneService {

    @Autowired
    MilestoneRepository repo;

    public Iterable<Milestone> list() {
        return repo.findAll();
    }

    public Milestone save(Milestone task) {
        return repo.save(task);
    }

    public Milestone show(Long id) {
        return repo.getOne(id);
    }
}
