package tasq.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tasq.domain.entity.Task;
import tasq.domain.repository.TaskRepository;

/**
 * @author ryotan
 * @since 1.0
 */
@Service
@Transactional
public class TaskService {

    @Autowired
    TaskRepository repo;

    public Iterable<Task> list() {
        return repo.findAll();
    }

    public Task save(Task task) {
        return repo.save(task);
    }

    public Task show(Long id) {
        return repo.getOne(id);
    }
}
