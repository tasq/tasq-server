package tasq.domain.validation.group;

/**
 * 更新時のみ実行する精査グループを表すインターフェース。
 *
 * @author ryotan
 * @since 1.0
 */
public interface Update {

}
