package tasq.security;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import tasq.security.SecurityConfiguration.CORSConfigurations;

@Component
public class CORSFilter implements Filter {

    Logger logger = LoggerFactory.getLogger(CORSFilter.class);

    @Autowired
    private CORSConfigurations config;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse) res;
        config.getHttpHeaders().forEachKeyValue(response::addHeader);

        if("OPTIONS".equals(((HttpServletRequest) req).getMethod())) {
            response.setStatus(HttpStatus.OK.value());
        } else {
            chain.doFilter(req, res);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("{}", config);
        // nop
    }

    @Override
    public void destroy() {
        // nop
    }
}

