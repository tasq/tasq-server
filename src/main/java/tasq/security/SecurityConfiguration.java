package tasq.security;

import javax.annotation.PostConstruct;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import com.gs.collections.api.map.ImmutableMap;
import com.gs.collections.impl.map.mutable.UnifiedMap;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * セキュリティ関連の設定を保持するConfigurationクラス。
 */
@ConfigurationProperties(prefix = "security")
public class SecurityConfiguration {

    /**
     * Cross Origin Resource Sharing (CORS) に関連する設定を保持するConfigurationクラス。
     * <p>
     * 設定プレフィックス: {@code security.cors}
     */
    @Component
    @ConfigurationProperties(prefix = "security.cors")
    @EqualsAndHashCode(exclude = "httpHeaders")
    @ToString(exclude = "httpHeaders")
    public static final class CORSConfigurations {

        /**
         * CORSリクエストを許容するホスト
         */
        @Setter
        private String allowedOrigin = "none";

        /**
         * CORSリクエストとして許容するHTTPメソッド
         */
        @Setter
        private String allowedMethods = "GET,POST,DELETE,OPTIONS";

        /**
         * CORSリクエストに許容するHTTPヘッダ
         */
        @Setter
        private String allowedHeaders =
            "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization";

        @Setter
        private String allowCredentials = "true";

        /**
         * CORSリクエストの有効期間
         */
        @Setter
        private String maxAge = String.valueOf(60 * 60);

        @Getter
        private ImmutableMap<String, String> httpHeaders;

        @PostConstruct
        public void initializeHeaders() {
            this.httpHeaders = UnifiedMap.<String, String>newMap(5)
                                         .withKeysValues("Access-Control-Allow-Origin", this.allowedOrigin)
                                         .withKeysValues("Access-Control-Allow-Methods", this.allowedMethods)
                                         .withKeysValues("Access-Control-Allow-Headers", this.allowedHeaders)
                                         .withKeysValues("Access-Control-Allow-Credentials", this.allowCredentials)
                                         .withKeysValues("Access-Control-Max-Age", this.maxAge)
                                         .toImmutable();
        }

        public CORSConfigurations() {
        }
    }
}
